package core

import model.Parameters
import view.GameView
import view.SettingsView

object Main {
    @JvmStatic
    fun main(args: Array<String>) {
        val connect = GameView()
        Parameters.runningGame = connect

        val settings = SettingsView()
        settings.setVisible(true)
    }
}
