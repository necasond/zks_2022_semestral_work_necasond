package controller

import model.Move
import model.Parameters

/**
 * This class contains game field and methods changing its state.
 */
class Game {
    var previousMove: Move
    var lastPlayer: Int
    var winner: Int
    var field: Array<IntArray>
    var isOverflow: Boolean
    var isGameOver: Boolean
    var turn: Int

    /**
     * Constructor
     */
    constructor() {
        previousMove = Move()
        lastPlayer = Parameters.Player2
        winner = 0
        field = Array(7) { IntArray(8) }
        isOverflow = false
        isGameOver = false
        turn = 0
        for (i in 0 until rows) {
            for (j in 0 until columns) {
                field[i][j] = 0
            }
        }
    }

    /**
     * Copy of constractor used for AI algorithms
     * @param game
     */
    constructor(game: Game) {
        previousMove = game.lastMove
        lastPlayer = game.lastPlayer
        winner = game.winner
        isOverflow = game.isOverflow
        isGameOver = game.isGameOver
        turn = game.turn
        val N1 = game.field.size
        val N2 = game.field[0].size
        field = Array(N1) { IntArray(N2) }
        for (i in 0 until N1) {
            for (j in 0 until N2) {
                field[i][j] = game.field[i][j]
            }
        }
    }

    /**
     * This method represents a move by given player into chosen column.
     * Using method getEmptyRowPosition decides in which row to input the block.
     * @param column
     * @param player
     */
    fun makeMove(column: Int, player: Int) {
        try {
            previousMove = Move(getEmptyRowPosition(column), column)
            lastPlayer = player
            field[getEmptyRowPosition(column)][column] = player
            turn++
            println("Turn: " + turn)
        } catch (e: ArrayIndexOutOfBoundsException) {
            System.err.println("Column " + (column + 1) + " is full!")
            fullColumn(true)
        }
    }

    /**
     * This method checks if given column is full or not
     * @param col
     * @return true if is is, false if isn't
     */
    fun checkFullColumn(col: Int): Boolean {
        return if (field[0][col] == 0) false else true
    }

    /**
     * This method returnes the mosistion of first empty row in a column
     * (because blocks are being built from the groung)
     * @param col
     * @return
     */
    fun getEmptyRowPosition(col: Int): Int {
        var rowPosition = -1
        for (row in 0 until rows) {
            if (field[row][col] == 0) {
                rowPosition = row
            }
        }
        return rowPosition
    }

    /**
     * This method is used when finding the winner, but more importantly
     * it is used in MinMax algorithm used by Computer player
     * @param n
     * @param player
     * @return frequency of player's block in a row
     */
    fun countBlocks(n: Int, player: Int): Int {
        var count = 0
        val r = blocksInRow(n, player)
        val c = blocksInColumn(n, player)
        val d = blocksDiagonally(n, player)
        count = r + c + d
        return count
    }

    /**
     * counting blocks in a row, horizontallt
     * @param n
     * @param player
     * @return count
     */
    fun blocksInRow(n: Int, player: Int): Int {
        var count = 0
        for (i in 0 until rows) {
            for (j in 0 until columns) {
                if (legalMove(i, j + inARow - 1)) {
                    // checking for n consecutive blocks of the same color in a row
                    var same = 0
                    while (same < n && field[i][j + same] == player) {
                        same++
                    }
                    if (same == n) {
                        while (same < inARow && (field[i][j + same] == player || field[i][j + same] == 0)) {
                            same++
                        }
                        if (same == inARow) count++
                    }
                }
            }
        }
        return count
    }

    /**
     * counting blocks of one color in column
     * @param n
     * @param player
     * @return count
     */
    fun blocksInColumn(n: Int, player: Int): Int {
        var count = 0
        for (i in 0 until rows) {
            for (j in 0 until columns) {
                if (legalMove(i - inARow + 1, j)) {
                    var same = 0
                    while (same < n && field[i - same][j] == player) {
                        same++
                    }
                    if (same == inARow) {
                        while (same < inARow && (field[i - same][j] == player || field[i - same][j] == 0)) {
                            same++
                        }
                        if (same == inARow) count++
                    }
                }
            }
        }
        return count
    }

    /**
     * counting n consecutive block in a diagonal
     * @param n
     * @param player
     * @return count
     */
    fun blocksDiagonally(n: Int, player: Int): Int {
        var count = 0
        for (i in 0 until rows) {
            for (j in 0 until columns) {
                if (legalMove(i + inARow - 1, j + inARow - 1)) {
                    var same = 0
                    while (same < n && field[i + same][j + same] == player) {
                        same++
                    }
                    if (same == inARow) {
                        while (same < inARow && (field[i + same][j + same] == player || field[i + same][j + same] == 0)) {
                            same++
                        }
                        if (same == inARow) count++
                    }
                }
            }
        }
        for (i in 0 until rows) {
            for (j in 0 until columns) {
                if (legalMove(i - inARow + 1, j + inARow - 1)) {
                    var same = 0
                    while (same < n && field[i - same][j + same] == player) {
                        same++
                    }
                    if (same == inARow) {
                        while (same < inARow && (field[i - same][j + same] == player || field[i - same][j + same] == 0)) {
                            same++
                        }
                        if (same == inARow) count++
                    }
                }
            }
        }
        return count
    }

    /**
     * This function makes sure that when searching through game board,
     * numbers stayes within boarders
     * @param row
     * @param column
     * @return true if given position is in board, false if not
     */
    fun legalMove(row: Int, column: Int): Boolean {
        return if (row <= -1 || column <= -1 || row > rows - 1 || column > columns - 1) {
            false
        } else true
    }

    fun isWon(): Boolean {
        /**
         * This method is used when checking if one of the players won the game.
         * @return true if one of players is a winner, false if not
         */
        val times4InARowPlayer1 = countBlocks(inARow, Parameters.Player1)
        if (times4InARowPlayer1 > 0) {
            winner = Parameters.Player1
            return true
        }

        val times4InARowPlayer2 = countBlocks(inARow, Parameters.Player2)
        if (times4InARowPlayer2 > 0) {
            winner = Parameters.Player2
            return true
        }

        winner = 0 // set nobody as the winner

        return false
    }

    fun isOver(): Boolean {
        /**
         * Checking if game is over or not.
         * @return true if game ended, false if not
         */
        return if (isWon()) {
            true
        } else isUndecided()
    }

    fun isUndecided(): Boolean {
        /**
         * Methods is checking for an empty position on the board.
         * If the whole board if full, game is over and undecided.
         * @return true if it is a draw, false if not
         */
        if (isGameOver) return false
        for (row in 0 until rows) {
            for (col in 0 until columns) {
                if (field[row][col] == 0) {
                    return false
                }
            }
        }
        return true
    }

    /**
     * This method is used for MinMax algorithm,
     * is generates the children of the current state of the game
     * The number of children equals to number of columns in the board
     * @param letter
     * @return ArrayList of children
     */
    fun getChildren(letter: Int): ArrayList<Game> {
        val children = ArrayList<Game>()
        for (col in 0 until columns) {
            if (!checkFullColumn(col)) {
                val child = Game(this)
                child.makeMove(col, letter)
                children.add(child)
            }
        }
        return children
    }

    /**
     * This method is used in MinMax algorithm.
     * It evluates the score of both players and returns its difference
     * It uses patern: 10^n for each (n + 2) consecutive blocks of one player - 10^n for each (n + 2) consecutive blocks of another player
     * @return difference of scores of both players
     */
    fun evaluate(): Int {
        var player1Score = 0
        var player2Score = 0
        if (isWon()) {
            if (winner == Parameters.Player1) player1Score =
                Math.pow(10.0, (inARow - 2).toDouble()).toInt() else if (winner == Parameters.Player2) player2Score =
                Math.pow(10.0, (inARow - 2).toDouble()).toInt()
        }
        for (i in 0 until inARow - 2) {
            player1Score += (countBlocks(i + 2, Parameters.Player1) * Math.pow(10.0, i.toDouble())).toInt()
            player2Score += (countBlocks(i + 2, Parameters.Player2) * Math.pow(10.0, i.toDouble())).toInt()
        }
        return player1Score - player2Score
    }

    val lastMove: Move
        get() = previousMove

    fun fullColumn(overflow: Boolean) {
        isOverflow = overflow
    }

    companion object {
        val rows: Int = Parameters.ROWS
        val columns: Int = Parameters.COLUMNS
        val inARow: Int = Parameters.IN_A_ROW

        /**
         * This method prints the fieldin the console
         * so that it could be seen even without GUI.
         * Empty positions are represented as '-'
         * Blocks of player one are represented as '1'
         * Blocks of player two are represented as '2'
         * @param field
         */
        fun printField(field: Array<IntArray>) {
            println("\n______________________________")
            println(" 1  2  3  4  5  6  7 ")
            println()
            for (i in 0 until rows) {
                for (j in 0 until columns) {
                    if (j != columns - 1) {
                        if (field[i][j] != 0) {
                            print(" " + field[i][j] + " ")
                        } else {
                            print(" " + "-" + " ")
                        }
                    } else {
                        if (field[i][j] != 0) {
                            println(" " + field[i][j] + " ")
                        } else {
                            println(" " + "-" + " ")
                        }
                    }
                }
            }
        }
    }
}