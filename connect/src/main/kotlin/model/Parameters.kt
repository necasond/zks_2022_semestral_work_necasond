package model

import view.GameView

object Parameters {
    //current running game
    var runningGame: GameView? = null

    //Board dimensions
    const val ROWS = 6
    const val COLUMNS = 7
    const val IN_A_ROW = 4

    // Players
    const val Player1 = 1
    const val Player2 = 2

    // Game modes
    const val HUMAN_VS_COMPUTER = 1
    const val HUMAN_VS_HUMAN = 2

    // Colors
    const val RED = 1
    const val YELLOW = 2
    const val BLUE = 3
    const val GREEN = 4
    const val ORANGE = 5
    const val PURPLE = 6
    const val WHITE = 7

    // Game default values
    var gameMode = HUMAN_VS_COMPUTER
    var difficulty = 4
    var player1Color = RED
    var player2Color = YELLOW
    fun getColorNameByNumber(number: Int): String {
        return when (number) {
            2 -> "YELLOW"
            3 -> "BLUE"
            4 -> "GREEN"
            5 -> "ORANGE"
            6 -> "PURPLE"
            7 -> "WHITE"
            else -> "RED"
        }
    }
}