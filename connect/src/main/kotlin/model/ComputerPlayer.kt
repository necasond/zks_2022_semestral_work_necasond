package model

import controller.Game
import java.util.*

class ComputerPlayer
/**
 * Constructor
 * @param maxD
 * @param aiLetter
 */(private val maxD: Int, val cpPlayer: Int) {

    /**
     * Method initiates MinMax Alpha Beta algorithm.
     * @param game
     * @return move of AI player
     */
    fun minMaxAB(game: Game): Move {
        return if (cpPlayer == Parameters.Player1) {
            maxAB(Game(game), 0, Double.MAX_VALUE, Double.MIN_VALUE)
        } else {
            minAlphaBeta(Game(game), 0, Double.MIN_VALUE, Double.MAX_VALUE)
        }
    }

    /**
     * Method calls max and min function alternately until it reaches maximum depth using maximize function
     * @param game
     * @param depth
     * @return min value move
     */
    fun minimize(game: Game, depth: Int): Move {
        val r = Random()
        if (game.isOver() || depth == maxD) {
            return Move(game.lastMove.row, game.lastMove.column, game.evaluate())
        }
        val children: ArrayList<Game> = ArrayList<Game>(game.getChildren(Parameters.Player2))
        val minMove = Move(Int.MAX_VALUE)
        for (child in children) {
            val move: Move = maximize(child, depth + 1)
            if (move.value <= minMove.value) {
                if (move.value === minMove.value) {
                    if (r.nextInt(2) == 0) {
                        minMove.value = (move.value)
                        minMove.row = (child.lastMove.row)
                        minMove.column = (child.lastMove.column)
                    }
                } else {
                    minMove.row = (child.lastMove.row)
                    minMove.column = (child.lastMove.column)
                    minMove.value = (move.value)
                }
            }
        }
        return minMove
    }

    /**
     * Method calls max and min function alternately until it reaches maximum depth similarly as minimize function
     * @param game
     * @param depth
     * @return max value move
     */
    fun maximize(game: Game, depth: Int): Move {
        val r = Random()
        if (game.isOver() || depth == maxD) {
            return Move(game.lastMove.row, game.lastMove.column, game.evaluate())
        }
        val children: ArrayList<Game> = ArrayList<Game>(game.getChildren(Parameters.Player1))
        val maxMove = Move(Int.MIN_VALUE)
        for (child in children) {
            val move: Move = minimize(child, depth + 1)
            if (move.value >= maxMove.value) {
                if (move.value === maxMove.value) {
                    if (r.nextInt(2) == 0) {
                        maxMove.value = (move.value)
                        maxMove.row = (child.lastMove.row)
                        maxMove.column = (child.lastMove.column)
                    }
                } else {
                    maxMove.row = (child.lastMove.row)
                    maxMove.column = (child.lastMove.column)
                    maxMove.value = (move.value)
                }
            }
        }
        return maxMove
    }

    /**
     * Method calls max and min function alternately until it reaches maximum depth
     * @param game
     * @param depth
     * @param a
     * @param b
     * @return max value move
     */
    fun maxAB(game: Game, depth: Int, a: Double, b: Double): Move {
        var a = a
        val r = Random()
        if (game.isOver() || depth == maxD) {
            return Move(game.lastMove.row, game.lastMove.column, game.evaluate())
        }
        val children: ArrayList<Game> = ArrayList<Game>(game.getChildren(Parameters.Player1))
        val maxMove = Move(Int.MIN_VALUE)
        for (child in children) {
            val move: Move = minAlphaBeta(child, depth + 1, a, b)
            if (move.value >= maxMove.value) {
                if (move.value === maxMove.value) {
                    if (r.nextInt(2) == 0) {
                        maxMove.row = (child.lastMove.row)
                        maxMove.column = (child.lastMove.column)
                        maxMove.value = (move.value)
                    }
                } else {
                    maxMove.row = (child.lastMove.row)
                    maxMove.column = (child.lastMove.column)
                    maxMove.value = (move.value)
                }
            }
            if (maxMove.value >= b) {
                return maxMove
            }
            a = if (a > maxMove.value) a else maxMove.value.toDouble()
        }
        return maxMove
    }

    /**
     * Method calls max and min function alternately until it reaches maximum depth
     * @param game
     * @param depth
     * @param a
     * @param b
     * @return min value move
     */
    fun minAlphaBeta(game: Game, depth: Int, a: Double, b: Double): Move {
        var b = b
        val r = Random()
        if (game.isOver() || depth == maxD) {
            return Move(game.lastMove.row, game.lastMove.column, game.evaluate())
        }
        val children: ArrayList<Game> = ArrayList<Game>(game.getChildren(Parameters.Player2))
        val minMove = Move(Int.MAX_VALUE)
        for (child in children) {
            val move: Move = maxAB(child, depth + 1, a, b)
            if (move.value <= minMove.value) {
                if (move.value === minMove.value) {
                    if (r.nextInt(2) == 0) {
                        minMove.row = (child.lastMove.row)
                        minMove.column = (child.lastMove.column)
                        minMove.value = (move.value)
                    }
                } else {
                    minMove.row = (child.lastMove.row)
                    minMove.column = (child.lastMove.column)
                    minMove.value = (move.value)
                }
            }
            if (minMove.value <= a) {
                return minMove
            }
            b = if (b < minMove.value) b else minMove.value.toDouble()
        }
        return minMove
    }
}