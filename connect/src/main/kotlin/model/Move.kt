package model

class Move {
    var row = 0
    var column = 0
    var value = 0

    /**
     * Constractor of object representing one move of a player
     */
    constructor()
    constructor(row: Int, col: Int) {
        this.row = row
        column = col
    }

    constructor(value: Int) {
        this.value = value
    }

    constructor(row: Int, col: Int, value: Int) {
        this.row = row
        column = col
        this.value = value
    }
}