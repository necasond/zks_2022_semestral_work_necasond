package view

import model.Parameters
import java.awt.Color
import java.awt.event.ActionEvent
import java.awt.event.ActionListener
import javax.swing.*

class SettingsView : JFrame("Settings") {
    private val gameModeLabel: JLabel
    private val difficultyLabel: JLabel
    private val player1ColorLabel: JLabel
    private val player2ColorLabel: JLabel
    private val game_mode_drop_down: JComboBox<*>
    private val difficulty_drop_down: JComboBox<*>
    private val player1_color_drop_down: JComboBox<*>
    private val player2_color_drop_down: JComboBox<*>
    private val play: JButton
    private val cancel: JButton
    private val handler: EventHandler

    init {
        defaultCloseOperation = DISPOSE_ON_CLOSE
        layout = null
        setSize(Companion.width, Companion.height)
        setLocationRelativeTo(null)
        isResizable = false
        handler = EventHandler()
        val selectedMode: Int = Parameters.gameMode
        val selectedDiff: Int = Parameters.difficulty
        val selectedColor1: Int = Parameters.player1Color
        val selectedColor2: Int = Parameters.player2Color
        gameModeLabel = JLabel("Game mode")
        difficultyLabel = JLabel("Level")
        player1ColorLabel = JLabel("Player 1 color")
        player2ColorLabel = JLabel("Player 2 color")
        add(gameModeLabel)
        add(difficultyLabel)
        add(player1ColorLabel)
        add(player2ColorLabel)
        game_mode_drop_down = JComboBox<Any?>()
        game_mode_drop_down.addItem("Human Vs Computer")
        game_mode_drop_down.addItem("Human Vs Human")
        if (selectedMode == Parameters.HUMAN_VS_COMPUTER) game_mode_drop_down.setSelectedIndex(Parameters.HUMAN_VS_COMPUTER - 1) else if (selectedMode == Parameters.HUMAN_VS_HUMAN) game_mode_drop_down.selectedIndex =
            Parameters.HUMAN_VS_HUMAN - 1
        difficulty_drop_down = JComboBox<Any?>()
        difficulty_drop_down.addItem(1)
        difficulty_drop_down.addItem(2)
        difficulty_drop_down.addItem(3)
        difficulty_drop_down.addItem(4)
        difficulty_drop_down.addItem(5)
        difficulty_drop_down.addItem(6)
        difficulty_drop_down.addItem(7)
        difficulty_drop_down.selectedIndex = selectedDiff
        player1_color_drop_down = JComboBox<Any?>()
        player1_color_drop_down.addItem("Red")
        player1_color_drop_down.addItem("Yellow")
        player1_color_drop_down.addItem("Blue")
        player1_color_drop_down.addItem("Green")
        player1_color_drop_down.addItem("Orange")
        player1_color_drop_down.addItem("Purple")
        player1_color_drop_down.addItem("White")
        if (selectedColor1 == Parameters.RED) player1_color_drop_down.setSelectedIndex(Parameters.RED - 1) else if (selectedColor1 == Parameters.YELLOW) player1_color_drop_down.setSelectedIndex(
            Parameters.YELLOW - 1
        ) else if (selectedColor1 == Parameters.BLUE) player1_color_drop_down.setSelectedIndex(Parameters.BLUE - 1) else if (selectedColor1 == Parameters.GREEN) player1_color_drop_down.setSelectedIndex(
            Parameters.GREEN - 1
        ) else if (selectedColor1 == Parameters.ORANGE) player1_color_drop_down.setSelectedIndex(Parameters.ORANGE - 1) else if (selectedColor1 == Parameters.PURPLE) player1_color_drop_down.setSelectedIndex(
            Parameters.PURPLE - 1
        ) else if (selectedColor1 == Parameters.WHITE) player1_color_drop_down.selectedIndex =
            Parameters.WHITE - 1
        player2_color_drop_down = JComboBox<Any?>()
        player2_color_drop_down.addItem("Red")
        player2_color_drop_down.addItem("Yellow")
        player2_color_drop_down.addItem("Blue")
        player2_color_drop_down.addItem("Green")
        player2_color_drop_down.addItem("Orange")
        player2_color_drop_down.addItem("Purple")
        player2_color_drop_down.addItem("White")
        if (selectedColor2 == Parameters.RED) player2_color_drop_down.setSelectedIndex(Parameters.RED - 1) else if (selectedColor2 == Parameters.YELLOW) player2_color_drop_down.setSelectedIndex(
            Parameters.YELLOW - 1
        ) else if (selectedColor2 == Parameters.BLUE) player2_color_drop_down.setSelectedIndex(Parameters.BLUE - 1) else if (selectedColor2 == Parameters.GREEN) player2_color_drop_down.setSelectedIndex(
            Parameters.GREEN - 1
        ) else if (selectedColor2 == Parameters.ORANGE) player2_color_drop_down.setSelectedIndex(Parameters.ORANGE - 1) else if (selectedColor2 == Parameters.PURPLE) player2_color_drop_down.setSelectedIndex(
            Parameters.PURPLE - 1
        ) else if (selectedColor2 == Parameters.WHITE) player2_color_drop_down.selectedIndex =
            Parameters.WHITE - 1
        add(game_mode_drop_down)
        add(difficulty_drop_down)
        add(player1_color_drop_down)
        add(player2_color_drop_down)
        gameModeLabel.setBounds(25, 55, 175, 20)
        difficultyLabel.setBounds(25, 85, 175, 20)
        player1ColorLabel.setBounds(25, 115, 175, 20)
        player2ColorLabel.setBounds(25, 145, 175, 20)
        game_mode_drop_down.setBounds(195, 55, 160, 20)
        difficulty_drop_down.setBounds(195, 85, 160, 20)
        player1_color_drop_down.setBounds(195, 115, 160, 20)
        player2_color_drop_down.setBounds(195, 145, 160, 20)
        play = JButton("Play")
        cancel = JButton("Cancel")
        add(play)
        add(cancel)
        play.setBounds(85, 230, 100, 30)
        play.addActionListener(handler)
        cancel.setBounds(195, 230, 100, 30)
        cancel.addActionListener(handler)
        background = Color.gray
        play.background = Color.darkGray
        play.foreground = Color.white
        cancel.background = Color.darkGray
        cancel.foreground = Color.white
    }

    private inner class EventHandler : ActionListener {
        override fun actionPerformed(ev: ActionEvent) {
            if (ev.source === cancel) {
                dispose()
            } else if (ev.source === play) {
                try {
                    val gameMode = game_mode_drop_down.selectedIndex + 1
                    val player1Color = player1_color_drop_down.selectedIndex + 1
                    val player2Color = player2_color_drop_down.selectedIndex + 1
                    if (player1Color == player2Color) {
                        JOptionPane.showMessageDialog(
                            null,
                            "You must chose different color for each player.",
                            "ERROR", JOptionPane.ERROR_MESSAGE
                        )
                        return
                    }

                    // Change game parameters based on settings.
                    Parameters.gameMode = gameMode
                    Parameters.difficulty = difficulty_drop_down.selectedIndex
                    Parameters.player1Color = player1Color
                    Parameters.player2Color = player2Color
                    dispose()
                    Parameters.runningGame?.start()
                } catch (e: Exception) {
                    System.err.println("ERROR : " + e.message)
                }
            }
        }
    }

    companion object {
        var width = 400
        var height = 320
    }
}