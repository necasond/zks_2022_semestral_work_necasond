package view

import controller.Game
import model.ComputerPlayer
import model.Move
import model.Parameters
import java.awt.*
import java.awt.event.WindowAdapter
import java.awt.event.WindowEvent
import java.util.*
import javax.swing.*

class GameView : JFrame(), Runnable {
    private val columns: Int = Parameters.COLUMNS
    private val frameWidth = 625
    private val frameHeight = 530
    private lateinit var ai: ComputerPlayer
    private lateinit var game: Game
    private var frame: JFrame? = null
    private lateinit var mainPanel: JPanel
    private var panelBoardNumbers: JPanel? = null
    private var buttonsPanel: JPanel? = null
    private lateinit var gameBoard: JLayeredPane
    private val buttons: Array<JButton?>

    // undo and redo method uses Stacks to store values of previous moves
    private val undoGames: Stack<Game> = Stack<Game>()
    private val undoBlockLabels = Stack<JLabel>()
    private val redoGames: Stack<Game> = Stack<Game>()
    private val redoBlockLabels = Stack<JLabel>()
    private var newGameButton: JButton? = null
    private var instructionButton: JButton? = null
    private var undoButton: JButton? = null
    private var redoButton: JButton? = null
    private var timer: JLabel? = null
    private var thread: Thread
    private var m = 0
    private var s = 0
    private val ms = 0
    private val sStr = ""
    private var nStr: String? = null
    private var running = false

    /**
     * constructor
     */
    init {
        buttons = arrayOfNulls(columns)
        for (i in 0 until columns) {
            buttons[i] = JButton(" ⯆ ")
            buttons[i]!!.isFocusable = false
            buttons[i]!!.background = Color.gray
            buttons[i]!!.foreground = Color.black
        }
        thread = Thread()
        thread.start()
    }

    /**
     * This methods is called every time a new game starts.
     */
    fun createNewGame() {
        try {
            UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName())
        } catch (e: ClassNotFoundException) {
            e.printStackTrace()
        } catch (e: IllegalAccessException) {
            e.printStackTrace()
        } catch (e: InstantiationException) {
            e.printStackTrace()
        } catch (e: UnsupportedLookAndFeelException) {
            e.printStackTrace()
        }
        unableButtons()
        game = Game()
        undoGames.clear()
        undoBlockLabels.clear()
        redoGames.clear()
        redoBlockLabels.clear()
        if (frame != null) frame!!.dispose()
        frame = JFrame("Build 4 Blocks")
        centerWindow(frame!!, frameWidth, frameHeight)
        val MainWindowContents = createContentComponents()
        frame!!.contentPane.add(MainWindowContents, BorderLayout.CENTER)
        frame!!.defaultCloseOperation = DISPOSE_ON_CLOSE
        frame!!.addWindowListener(object : WindowAdapter() {
            override fun windowClosing(e: WindowEvent) {
                System.exit(0)
            }
        })
        frame!!.isFocusable = true
        frame!!.pack()
        val tools = JToolBar()
        tools.isFloatable = false
        frame!!.add(tools, BorderLayout.PAGE_END)
        frame!!.isVisible = true
        Game.printField(game!!.field)
        running = true
        if (Parameters.gameMode === Parameters.HUMAN_VS_COMPUTER) {
            ai = ComputerPlayer(Parameters.difficulty, Parameters.Player2)
        }
    }

    /**
     * This method creates the main game board
     *
     * @return gameBoard with loaded image of game board
     */
    private fun createGameBoard(): JLayeredPane {
        gameBoard = JLayeredPane()
        gameBoard!!.preferredSize = Dimension(frameWidth, frameHeight)
        gameBoard!!.border = null
        gameBoard!!.background = Color.darkGray
        val input = GameView::class.java.getResource("/images/Board.png")
        val imageBoard = ImageIcon(input)
        val imageBoardLabel = JLabel(imageBoard)
        imageBoardLabel.setBounds(20, 20, imageBoard.iconWidth, imageBoard.iconHeight)
        gameBoard!!.add(imageBoardLabel, 0, 1)
        return gameBoard
    }

    /**
     * This method unables player to undo his move (take his move back).
     */
    private fun undo() {
        if (!undoGames.isEmpty()) {
            // undo implementation for Human vs Human mode
            if (Parameters.gameMode === Parameters.HUMAN_VS_HUMAN) {
                try {
                    game?.isGameOver = (false)
                    unableButtons()
                    val previousBlockLabel = undoBlockLabels.pop()
                    redoGames.push(game?.let { Game(it) })
                    redoBlockLabels.push(previousBlockLabel)
                    game = undoGames.pop()
                    gameBoard!!.remove(previousBlockLabel)
                    frame!!.paint(frame!!.graphics)
                } catch (ex: ArrayIndexOutOfBoundsException) {
                    System.err.println("Undo cannot be used, no move has been made yet!")
                    System.err.flush()
                }
            } else if (Parameters.gameMode === Parameters.HUMAN_VS_COMPUTER) {
                try {
                    game.isGameOver = (false)
                    unableButtons()
                    val previousAiBlockLabel = undoBlockLabels.pop()
                    val previousHumanBlockLabel = undoBlockLabels.pop()
                    redoGames.push(game?.let { Game(it) })
                    redoBlockLabels.push(previousAiBlockLabel)
                    redoBlockLabels.push(previousHumanBlockLabel)
                    game = undoGames.pop()
                    gameBoard!!.remove(previousAiBlockLabel)
                    gameBoard!!.remove(previousHumanBlockLabel)
                    frame!!.paint(frame!!.graphics)
                } catch (e: NullPointerException) {
                    System.err.println("Undo cannot be used, no move has been made yet!")
                    System.err.flush()
                } catch (ex: ArrayIndexOutOfBoundsException) {
                    System.err.println("Undo cannot be used, no move has been made yet!")
                    System.err.flush()
                }
            }
            if (undoGames.isEmpty()) {
                undoButton!!.isEnabled = false
            }
            redoButton!!.isEnabled = true
            System.out.println("Turn: " + game.turn)
            Game.printField(game.field)
        }
    }

    /**
     * This method unables player to get beck previously returned move.
     */
    private fun redo() {
        if (!redoGames.isEmpty()) {
            // redo implementation for Human vs Human mode
            if (Parameters.gameMode === Parameters.HUMAN_VS_HUMAN) {
                try {
                    game.isGameOver = (false)
                    unableButtons()
                    val redoBlockLabel = redoBlockLabels.pop()
                    undoGames.push(Game(game))
                    undoBlockLabels.push(redoBlockLabel)
                    game = Game(redoGames.pop())
                    gameBoard!!.add(redoBlockLabel, 0, 0)
                    frame!!.paint(frame!!.graphics)
                    val isGameOver: Boolean = game.isOver()
                    if (isGameOver) {
                        gameOver()
                    }
                } catch (ex: ArrayIndexOutOfBoundsException) {
                    System.err.println("Redo cannot be used, there is no move to redo!")
                    System.err.flush()
                }
            } else if (Parameters.gameMode === Parameters.HUMAN_VS_COMPUTER) {
                try {
                    game.isGameOver = (false)
                    unableButtons()
                    val redoAiBlockLabel = redoBlockLabels.pop()
                    val redoHumanBlockLabel = redoBlockLabels.pop()
                    undoGames.push(Game(game))
                    undoBlockLabels.push(redoAiBlockLabel)
                    undoBlockLabels.push(redoHumanBlockLabel)
                    game = Game(redoGames.pop())
                    gameBoard!!.add(redoAiBlockLabel, 0, 0)
                    gameBoard!!.add(redoHumanBlockLabel, 0, 0)
                    frame!!.paint(frame!!.graphics)
                    val isGameOver: Boolean = game.isOver()
                    if (isGameOver) {
                        gameOver()
                    }
                } catch (e: NullPointerException) {
                    System.err.println("Redo cannot be used, there is no move to redo!")
                    System.err.flush()
                } catch (ex: ArrayIndexOutOfBoundsException) {
                    System.err.println("Undo cannot be used, no move has been made yet!")
                    System.err.flush()
                }
            }
            if (redoGames.isEmpty()) redoButton!!.isEnabled = false
            undoButton!!.isEnabled = true
            System.out.println("Turn: " + game.turn)
            Game.printField(game.field)
        }
    }

    /**
     * This method places block od given color on chosen position  on the game board.
     *
     * @param color
     * @param row
     * @param col
     */
    private fun placeBlock(color: Int, row: Int, col: Int) {
        val colorString: String = Parameters.getColorNameByNumber(color)
        val input = "connect/src/main/resources/images/$colorString.png"
        val BlockIcon = ImageIcon(input)
        val BlockLabel = JLabel(BlockIcon)
        BlockLabel.setBounds(27 + 75 * col, 27 + 75 * row, BlockIcon.iconWidth, BlockIcon.iconHeight)
        gameBoard.add(BlockLabel, 0, 0)
        undoBlockLabels.push(BlockLabel)
    }

    /**
     * This method is called after MakeMove method.
     * Method updates game board as well as undo and redo buttoms.
     *
     * @return boolean value of method isGameOver()
     */
    private fun gameState(): Boolean {
        val row: Int = game.lastMove.row
        val col: Int = game.lastMove.column
        val currentPlayer: Int = game.lastPlayer
        if (currentPlayer == Parameters.Player1) {
            //places a Block in the corresponding position of the GUI.
            placeBlock(Parameters.player1Color, row, col)
        }
        if (currentPlayer == Parameters.Player2) {
            placeBlock(Parameters.player2Color, row, col)
        }
        System.out.println("Turn: " + game.turn)
        Game.printField(game.field)
        val isGameOver: Boolean = game.isOver()
        if (isGameOver) {
            gameOver()
        }
        undoButton!!.isEnabled = true
        redoGames.clear()
        redoBlockLabels.clear()
        redoButton!!.isEnabled = false
        return isGameOver
    }

    /**
     * This method is called in Human vs Computer mode, after human player makes his move.
     * Method uses MinMax algorithm to makes its move.
     *
     * @param ai represents computer player
     */
    private fun computerMove(ai: ComputerPlayer) {
        val aiMove: Move = ai.minMaxAB(game)
        game.makeMove(aiMove.column, ai.cpPlayer)
        gameState()
    }

    /**
     * This method disables all buttoms when the game is over
     */
    private fun disableButtons() {
        for (button in buttons) {
            for (actionListener in button!!.actionListeners) {
                button.removeActionListener(actionListener)
            }
        }
    }

    /**
     * This method unables buttons that are disabled.
     * It is used in undo and redo methods.
     */
    private fun unableButtons() {
        for (i in buttons.indices) {
            val button = buttons[i]
            if (button!!.actionListeners.size == 0) {
                button.addActionListener {
                    undoGames.push(Game(game))
                    makeMove(i)
                    if (!game.isOverflow) {
                        val isGameOver = gameState()
                        if (Parameters.gameMode === Parameters.HUMAN_VS_COMPUTER && !isGameOver) {
                            computerMove(ai)
                        }
                    }
                    frame!!.requestFocusInWindow()
                }
            }
        }
    }

    /**
     * This method is used  to find which player has next move
     *
     * @param col
     */
    private fun makeMove(col: Int) {
        game.fullColumn(false)
        val previousRow: Int = game.lastMove.row
        val previousCol: Int = game.lastMove.column
        val previousLetter: Int = game.lastPlayer
        if (game.lastPlayer === Parameters.Player2) {
            game.makeMove(col, Parameters.Player1)
        } else {
            game.makeMove(col, Parameters.Player2)
        }
        if (game.isOverflow) {
            game.lastMove.row = (previousRow)
            game.lastMove.column = (previousCol)
            game.lastPlayer = (previousLetter)
            undoGames.pop()
        }
    }

    /**
     * Method centers the frame in the middle of screen
     * @param frame
     * @param width
     * @param height
     */
    private fun centerWindow(frame: Window, width: Int, height: Int) {
        val dimension = Toolkit.getDefaultToolkit().screenSize
        val x = (dimension.getWidth() - frame.width - width).toInt() / 2
        val y = (dimension.getHeight() - frame.height - height).toInt() / 2
        frame.setLocation(x, y)
    }

    /**
     * This method creates components of mainPanel,
     * it declares their properties as well as call actionListener function
     *
     * @return mailPanel with all the components added to it
     */
    private fun createContentComponents(): Component {
        //creates panel for buttons menu
        buttonsPanel = JPanel()
        buttonsPanel = JPanel()
        buttonsPanel!!.layout = GridLayout(4, 1, 0, 15)
        buttonsPanel!!.border = BorderFactory.createEmptyBorder(22, 0, 400, 50)
        buttonsPanel!!.background = Color.darkGray

        //creates menu buttons
        newGameButton = JButton("New Game")
        instructionButton = JButton("Instructions")
        undoButton = JButton("Undo move")
        redoButton = JButton("Redo move")
        newGameButton!!.background = Color.gray
        instructionButton!!.background = Color.gray
        undoButton!!.background = Color.lightGray
        redoButton!!.background = Color.lightGray
        undoButton!!.isEnabled = false
        redoButton!!.isEnabled = false
        newGameButton!!.addActionListener {
            val settings = SettingsView()
            settings.setVisible(true)
        }
        undoButton!!.addActionListener { undo() }
        redoButton!!.addActionListener { redo() }
        instructionButton!!.addActionListener {
            JOptionPane.showMessageDialog(
                null,
                """
                     Click on the buttons above the game board to build a new block.
                     You may undo or redo your moves during the game.
                     In order to win, you must build 4 block in an row (horizontally, vertically or diagonally).
                     """.trimIndent(),
                "Instruction", JOptionPane.INFORMATION_MESSAGE
            )
        }

        //add menu buttons to the panel
        buttonsPanel!!.add(newGameButton)
        buttonsPanel!!.add(instructionButton)
        buttonsPanel!!.add(undoButton)
        buttonsPanel!!.add(redoButton)

        //creates a panel for board buttons
        panelBoardNumbers = JPanel()
        panelBoardNumbers!!.layout = GridLayout(1, columns, 4, 0)
        panelBoardNumbers!!.border = BorderFactory.createEmptyBorder(25, 22, 20, 226)
        panelBoardNumbers!!.background = Color.darkGray


        //adds them to the panel
        for (button in buttons) {
            button!!.preferredSize = Dimension(50, 20)
            panelBoardNumbers!!.add(button)
        }

        // creates main game board
        gameBoard = createGameBoard()

        //creates timer label
        timer = JLabel("00:00")
        timer!!.setBounds(640, 280, 150, 70)
        timer!!.font = Font(Font.SANS_SERIF, Font.BOLD, 30)
        timer!!.foreground = Color.lightGray
        timer!!.isVisible = true
        frame!!.add(timer)


        // creates panel for storing all the elements of game board
        mainPanel = JPanel()
        mainPanel!!.layout = BorderLayout()
        mainPanel!!.border = BorderFactory.createEmptyBorder(5, 5, 5, 5)
        mainPanel!!.background = Color.darkGray

        // adds all components to mainPanel
        mainPanel!!.add(panelBoardNumbers, BorderLayout.NORTH)
        mainPanel!!.add(gameBoard, BorderLayout.CENTER)
        mainPanel!!.add(buttonsPanel, BorderLayout.EAST)
        frame!!.isResizable = false
        frame!!.background = Color.darkGray
        return mainPanel
    }

    /**
     * This method is called a after checking with method isOver() from class 'Game',
     * therefore it is only called when the game is actually over
     */
    private fun gameOver() {
        game.isGameOver = (true)
        undoGames.clear()
        undoBlockLabels.clear()
        redoGames.clear()
        redoBlockLabels.clear()
        running = false
        disableButtons()
        stop()
        var choice = 0
        if (game.winner === Parameters.Player1) {
            if (Parameters.gameMode === Parameters.HUMAN_VS_COMPUTER) {
                choice = JOptionPane.showConfirmDialog(
                    null, "You won! Start a new game?",
                    "Game Over", JOptionPane.YES_NO_OPTION
                )
            }
            if (Parameters.gameMode === Parameters.HUMAN_VS_HUMAN) {
                choice = JOptionPane.showConfirmDialog(
                    null, "Player 1 won! Start a new game?",
                    "Game Over", JOptionPane.YES_NO_OPTION
                )
            }
        } else if (game.winner === Parameters.Player2) {
            if (Parameters.gameMode === Parameters.HUMAN_VS_COMPUTER) {
                choice = JOptionPane.showConfirmDialog(
                    null, "Computer player won! Start a new game?",
                    "Game Over", JOptionPane.YES_NO_OPTION
                )
            }
            if (Parameters.gameMode === Parameters.HUMAN_VS_HUMAN) {
                choice = JOptionPane.showConfirmDialog(
                    null, "Player 2 won! Start a new game?",
                    "Game Over", JOptionPane.YES_NO_OPTION
                )
            }
        } else if (game.isUndecided()) {
            choice = JOptionPane.showConfirmDialog(
                null, "Game ended in a tie. Start a new game?",
                "Game Over", JOptionPane.YES_NO_OPTION
            )
        }
        if (choice == JOptionPane.YES_OPTION) {
            val settings = SettingsView()
            settings.setVisible(true)
        }
        disableButtons()
    }

    /**
     * Method resets the stopwatch before each game.
     */
    private fun reset() {
        s = 0
        m = s
        nStr = "00:00"
        display()
    }

    /**
     * Method displays stopwatch time.
     */
    private fun display() {
        timer!!.text = nStr
    }

    /**
     * Method adjusts the time format for displaying.
     */
    private fun setCounter() {
        nStr = ""
        nStr += if (m < 10) {
            "0$m"
        } else {
            "" + m
        }
        nStr += if (s < 10) {
            ":0$s"
        } else {
            ":$s"
        }
    }

    /**
     * Method starts the game.
     */
    @Synchronized
    fun start() {
        if (running) return
        running = true
        thread = Thread(this)
        thread.start()
    }

    /**
     * Method stops the game.
     */
    @Synchronized
    fun stop() {
        if (!running) return
        running = false
        try {
            thread.join()
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }
    }

    override fun run() {
        createNewGame()
        reset()
        val startingTime = System.currentTimeMillis()
        try {
            while (running) {
                val time = (System.currentTimeMillis() - startingTime) / 1000
                s = time.toInt()
                if (s > 59) {
                    s = 0
                    m++
                }
                if (m > 59) {
                    var timesup = 0
                    reset()
                    timesup = JOptionPane.showConfirmDialog(
                        null,
                        """
                            You reached game time limit.
                            Now, you can either start a new game or exit the application.Do you want to start a new game?
                            """.trimIndent(),
                        "Time's up", JOptionPane.YES_NO_OPTION
                    )
                    if (timesup == JOptionPane.YES_OPTION) {
                        val settings = SettingsView()
                        settings.setVisible(true)
                    }
                    if (timesup == JOptionPane.NO_OPTION) {
                        System.exit(0)
                    }
                }
                setCounter()
                display()
                //Thread.sleep(1);
            }
        } catch (e: Exception) {
        }
        //stop();
    }
}