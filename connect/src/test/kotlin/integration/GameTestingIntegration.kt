package integration

import controller.Game
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class GameTestingIntegration {

    @Test
    fun `Game make moves - player wins`() {
        val game = Game()

        game.makeMove(5, 1)
        game.makeMove(5, 1)
        game.makeMove(5, 1)
        game.makeMove(5, 1)

        Assertions.assertEquals(true, game.isWon())
        Assertions.assertEquals(true, game.isOver())
        Assertions.assertEquals(1, game.winner)
    }

    @Test
    fun `Game make moves - ends undecided`() {
        val game = Game()

        for (i in 0..6) {
            for (j in 0..5) {
                game.makeMove(i, j % 2 + 1)
            }
        }

        Assertions.assertEquals(true, game.isUndecided())
        Assertions.assertEquals(true, game.isOver())
        for (k in 0..6) {
            Assertions.assertTrue(game.checkFullColumn(k))
        }
    }
}