package suites

import integration.GameTestingIntegration
import org.junit.platform.suite.api.SelectClasses
import org.junit.platform.suite.api.Suite
import org.junit.runner.JUnitCore
import org.junit.runner.RunWith
import parametrized.GameTestingParametrized
import unit.GameTestingUnit

@Suite
@SelectClasses(GameTestingUnit::class, GameTestingIntegration::class, GameTestingParametrized::class)
class AllTestsSuite {

}

@Suite
@SelectClasses(GameTestingUnit::class, GameTestingIntegration::class)
class UnitIntegrationSuite {

}