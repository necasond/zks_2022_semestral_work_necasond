package unit

import controller.Game
import org.junit.Assert.assertEquals
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers
import org.mockito.Mockito
import java.io.PrintStream


class GameTestingUnit {

    @Test
    fun `Make a move - add 1 block - new block on board`() {

        val game = Game()
        val row: Int = game.getEmptyRowPosition(5)

        game.makeMove(5, 1)

        Assertions.assertEquals(1, game.lastPlayer)
        Assertions.assertEquals(1, game.turn)
        Assertions.assertEquals(1, game.field[row][5])
        Assertions.assertEquals(5, game.lastMove.column)
    }

    @Test
    @Throws(ArrayIndexOutOfBoundsException::class)
    fun `Make a move - full column - error message`() {

        val game = Game()
        val error = "Column 6 is full!"
        val err = Mockito.mock(PrintStream::class.java)

        game.makeMove(5, 1)
        game.makeMove(5, 2)
        game.makeMove(5, 1)
        game.makeMove(5, 2)
        game.makeMove(5, 1)
        game.makeMove(5, 2)
        System.setErr(err)
        game.makeMove(5, 1)

        Mockito.verify(err).println(ArgumentMatchers.contains(error))
    }
}