package parametrized

import controller.Game
import org.junit.Assert.assertEquals
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.CsvSource

class GameTestingParametrized {
    @ParameterizedTest(name = "Player {0} added block to column number {1}.")
    @CsvSource("1,1", "2,2", "1,1", "2,1", "6,2", "4,2", "1,1", "5,2")
    fun `Make a move - add block - block added to board`(column: Int, player: Int) {
        val game = Game()

        game.makeMove(column, player)

        Assertions.assertEquals(player, game.lastPlayer)
        Assertions.assertEquals(column, game.lastMove.column)
    }
}