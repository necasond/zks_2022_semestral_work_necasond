# Path tests
## System run

![](PathTest1.png)

## Testing scenarios
Always starts in state #1

### 1# Game ended, player refused to start a new game
Sequence:
- #1 -> #2

### #2 Game ended, player chooses a new game, inputs wrong values
Sequence:
- #1 -> #3 -> #5 -> #3

### #3 Game ended, player chooses a new game, inputs correct values
Sequence:
- #1 -> #3 -> #4
