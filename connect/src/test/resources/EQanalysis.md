# Equivalence class analysis

For this analysis was used the setting formular

![](Setting.png)

There are 4 inputs. Because all inputs are discrete they are all valid.

![](EQanalysis.png)

However whole evaluation is parsed into 2 equivalence classes:
- Valid EC      (P1 & P2 have different colors)
- Invalid EC    (P1 & P2 have the same color)

