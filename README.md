# ZKS 2022 SEMESTRAL WORK NECASOND

In this project I tried to implement and test basic paradigms and methods
in software quality assurance

## Project workflow
### VCS
Organization was led through GitLab. Every new feature was introduced in separate feature branch.
Upon merge request the whole feature branch is squashed and merged into main. Basic GitLab CI/CD 
pipeline was set up for build and testing stages.

### Environment
Frontend is coded in React/Next.js framework controlled with npm manager. Rest of the repository
is based on Kotlin with Gradle manager.

### Test frameworks and used methods
- Cypress
  - E2E
  - Component
- JUnit
  - Parametrized tests
  - Unit tests
  - Integration tests
- ESlinter
- ACTS
- Path-based use cases
- EC analysis
- Test grouping

## Project components
There are 3 areas that were crated in this project to test various approaches
- connect - Kotlin application for JUnit testing
- frontend - Next.js frontend app for Cypress testing
- backend - SpringBoot app with openAPI specification for full-stack integration testing

## Connect component

Basic game where the player must build 4 squares in a row/column/diagonal to win.

![](connect/src/test/resources/Connect.png)

### Equivalence class analysis

For this analysis was used the setting formular

![](connect/src/test/resources/Setting.png)

There are 4 inputs. Because all inputs are discrete they are all valid.

![](connect/src/test/resources/EQanalysis.png)

However whole evaluation is parsed into 2 equivalence classes:
- Valid EC      (P1 & P2 have different colors)
- Invalid EC    (P1 & P2 have the same color)

### Advanced Combinatorial Testing System

On the same setting formular was performed ACTS through pair-wise technique.
Resulted combinatorial output:

![](connect/src/test/resources/ACTS.png)

### Path tests
#### System run
The setup logic aside from the core playable aspect can be divided into 
5 nodes.
- Game ended
- Game finished - End position when the application is closed
- Setting - Start position when the application is run
- New game
- Error warning about incorrect input

![](connect/src/test/resources/PathTest1.png)

#### Testing scenarios
Always starts in state #1 Game ended

#### 1# Game ended, player refused to start a new game
Sequence:
- #1 -> #2

#### #2 Game ended, player chooses a new game, inputs wrong values
Sequence:
- #1 -> #3 -> #5 -> #3

#### #3 Game ended, player chooses a new game, inputs correct values
Sequence:
- #1 -> #3 -> #4

### JUnit tests
Basic unit/integration/parametrized tests were introduced and grouped
into testing suites.

## Frontend component

Frontend was created in Next.js framework with implementation of Cypress 
testing environment.

Basic E2E and component tests were introduced.

### E2E
![](frontend/resources/e2e.png)

### Component
![](frontend/resources/component.png)

## Backend component & unfinished features
Backend was meant to connect with frontend through Spring boot. REST API 
endpoint was generated through openAPI specification, but unfortunately the 
changes were lost during unstashed checkout to other branch.

Ktlint checking/formatting was planned for Kotlin to keep coding style
the same between modules.

There were preparations for MongoDB as development & testing environments 
databases.

Docker deployment on local or remote k8s cluster. Controlled through k9s.

Atlassian Jira integration for VCS a project management.

## Summary
Even though this project does not have all the features that were initially 
planned included in itself, it still does showcase a rather wider range of usable paradigms
and methods from quality assurance.

The biggest setback was due to the vast number of technologies used the learning
curve and development process exceeded the formerly planned timetable and thus 
led to incomplete project.