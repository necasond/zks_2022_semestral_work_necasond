# Used libs
- Material UI
```bash
npm install @mui/material @emotion/react @emotion/styled
npm install @mui/icons-material
```
- Roboto font
```bash
npm install @fontsource/roboto
```
- Testing with Cypress
```bash
npm install --save-dev cypress
npm install --save-dev @cypress/grep
npm install --save-dev start-server-and-test
```