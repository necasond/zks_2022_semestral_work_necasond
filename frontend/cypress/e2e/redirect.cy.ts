export {}

describe('Opens the home page', () => {
    it('resource home passes', () => {
        cy.visit("localhost:3000/home")
    })

    it('empty resource directs to home', () => {
        cy.visit("localhost:3000/")

        cy.url().should('eq', 'http://localhost:3000/home');
    })

    it('invalid resource leads to 404', () => {
        cy.visit("localhost:3000/not-a-valid-resource-404", { failOnStatusCode: false})

        cy.get('body').contains('404')
        cy.get('body').contains('Resource Not found')
    })

    it('404 has link to home', () => {
        cy.visit("localhost:3000/not-a-valid-resource-404", { failOnStatusCode: false})

        cy.get('body')
            .find('a[href="/"]')
            .click();

        cy.url().should('eq', 'http://localhost:3000/home');
    })
})
