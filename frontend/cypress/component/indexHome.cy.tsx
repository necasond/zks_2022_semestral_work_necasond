import React from 'react'
import Home from '@/pages/home'

describe('Log in/Home page setup', () => {
  // Firstly we must see it
  it('renders',() => {
    cy.mount(<Home />)
  })
  // # is for ID
  it('contains log in elements', () => {
    cy.mount(<Home />)

    cy.get("#username")
    cy.get("#password")

    cy.get('button')
        .should('have.length', 1)
        .should('have.attr', 'type', 'submit');
  })
})