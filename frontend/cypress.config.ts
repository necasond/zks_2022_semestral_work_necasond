import { defineConfig } from "cypress";

export default defineConfig({
    component: {
        devServer: {
            framework: "next",
            bundler: "webpack",
        },
        specPattern: [
            'cypress/component/'
        ]
    },

    e2e: {
        setupNodeEvents(on, config) {
            require('@cypress/grep/src/plugin')(config);
            return config;
        },
        specPattern: [
            'cypress/e2e/'
        ],
    },

    video: false,
    screenshotOnRunFailure: false,
});
